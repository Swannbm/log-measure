from django.urls import include, path
from rest_framework.routers import DefaultRouter

from board import views

app_label = "board"


router = DefaultRouter()
router.register(r'measures', views.MeasureViewSet, basename="measure")


urlpatterns = [
    path("", views.HomeView.as_view(), name="home"),
    path("api/", include(router.urls)),
]
