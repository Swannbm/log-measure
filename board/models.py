from django.db import models


class Measure(models.Model):
    """Measure model."""
    date_created = models.DateTimeField(auto_now_add=True)
    board_name = models.CharField(max_length=25)
    temperature = models.FloatField(null=True, blank=True)
    humidity = models.FloatField(null=True, blank=True)
    heater = models.FloatField(null=True, blank=True)

    class Meta:
        verbose_name = 'Mesure'
        verbose_name_plural = 'Mesures'
        ordering = ["-date_created"]

    def __str__(self):
        return self.name
