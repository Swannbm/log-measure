from rest_framework import serializers

from board.models import Measure


class MeasureSerializer(serializers.ModelSerializer):
    """Measure serializer."""

    class Meta:
        """Meta class."""

        model = Measure
        fields = [
            "id",
            "board_name",
            "temperature",
            "humidity",
            "heater",
            "date_created",
        ]
        read_only_fields = ["date_created"]
