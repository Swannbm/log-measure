from django.views.generic.base import TemplateView
from rest_framework import viewsets

from board.models import Measure
from board.serializers import MeasureSerializer


class HomeView(TemplateView):
    template_name = "board/home.html"


class MeasureViewSet(viewsets.ModelViewSet):
    queryset = Measure.objects.all()
    serializer_class = MeasureSerializer
