from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from user.forms import CustomUserChangeForm, CustomUserCreationForm
from user.models import User


@admin.register(User)
class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = User
    list_display = ("email", "is_staff", "is_active")
    list_filter = ("is_staff", "is_active")
    readonly_fields = ("date_joined", "last_login")
    fieldsets = (
        (
            None,
            {"fields": ("first_name", "last_name", "email")},
        ),
        (
            "Password",
            {"fields": ("password",)},
        ),
        (
            "Permissions",
            {"fields": ("is_active", "is_staff", "is_superuser")},
        ),
        (
            "Audit",
            {"fields": ("date_joined", "last_login")},
        ),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "first_name",
                    "last_name",
                    "email",
                    "password1",
                    "password2",
                    "is_staff",
                    "is_active",
                    "is_superuser",
                ),
            },
        ),
    )
    search_fields = ("email",)
    ordering = ("email",)
