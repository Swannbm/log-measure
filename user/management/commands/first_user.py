from django.core.management.base import BaseCommand

from user.models import User


class Command(BaseCommand):
    help = "Test get image from highchart"

    def handle(self, *args, **options):
        User.objects.create_superuser(
            email="swann.bouviermuller@gmail.com",
            password="1234",
            first_name="Swann",
            last_name="Bouvier-Muller",
        )
