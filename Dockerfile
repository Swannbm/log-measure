FROM python:3.11

LABEL maintainer="swann.bouviermuller@gmail.com"
LABEL vendor="Innov & Code"

ENV POETRY_VERSION=1.3.2

RUN apt update

# upgrade pip
RUN pip install --upgrade pip
RUN pip install debugpy -t /tmp
RUN pip install "poetry==$POETRY_VERSION"

RUN mkdir -p /app

# install project dependencies
# copy only pipfile to leverage docker cache
COPY poetry.lock pyproject.toml /app/
WORKDIR /app
RUN poetry config virtualenvs.create false \
  && poetry install --no-interaction --no-ansi


EXPOSE 8080

CMD [ "python", "manage.py", "runserver", "0.0.0.0:8000" ]
